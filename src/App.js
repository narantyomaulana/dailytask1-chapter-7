import Card from './components/Card/Card';
import Navbar from './components/Navbar/Navbar';
import Header from './components/Header/Header';
import 'bootstrap/dist/css/bootstrap.min.css'
import './App.css';

function App() {
  return (
    <div className="App">
      <Navbar 
      title = "Hello FSW-13"
      home = "Home"
      about = "About"
      blog = "Blog"
      />
      <Header 
      title = "Welcome To My React Website"
      />
      <Card
        title="Hello"
        description="Lorem ipsum dolor sit amet"
        btnText="Go Somewhere"
        btnHref="https://google.com"
        imgSrc="https://placeimg.com/320/240/any"
        imgAlt="Hello"
      />
    </div>
  );
}

export default App;
