import React from 'react'

const Navbar = (props) => {
  const {title, home, about, blog}= props
  return (
    <div className="container-fluid">
      <div className="row">
        <div className="navbar navbar-expand-lg navbar-dark bg-dark p-3">
          <div className="container" >
            <a className = "navbar-brand" style={{ marginRight: "65%"}} href="{}">{title}</a>
            <div className="collapse navbar-collapse" id="navbarNav">
              <ul className="navbar-nav" >
                <li className="nav-item"  >
                  <a className="nav-link" aria-current="page" href="{}">{home}</a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" href="{}">{about}</a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" href="{}">{blog}</a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Navbar;