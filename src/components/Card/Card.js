import React from "react";
import Button from "../Button/Button";
const Card = (props) => {
  const { title, description, imgSrc, imgAlt, btnText } = props;

  return (
    <div className="row justify-content-center">
      <div className="card" style={{ width: "18rem", marginTop:"20px", borderRadius:"10px" }}>
        <img src={imgSrc} className="card-img-top" alt={imgAlt} />
        <div className="card-body">
          <h5 className="card-title">{title}</h5>
          <p className="card-text">{description}</p>
          <Button variant="black">{btnText}</Button>
        </div>
      </div>
      <div className="card" style={{ width: "18rem", marginTop:"20px", marginLeft:"10px", borderRadius:"10px"}}>
        <img src={imgSrc} className="card-img-top" alt={imgAlt} />
        <div className="card-body">
          <h5 className="card-title">{title}</h5>
          <p className="card-text">{description}</p>
          <Button variant="black">{btnText}</Button>
        </div>
      </div>
      <div className="card" style={{ width: "18rem", marginTop:"20px", marginLeft:"10px", borderRadius:"10px" }}>
        <img src={imgSrc} className="card-img-top" alt={imgAlt} />
        <div className="card-body">
          <h5 className="card-title">{title}</h5>
          <p className="card-text">{description}</p>
          <Button variant="black">{btnText}</Button>
        </div>
      </div>
    </div>
  )
}

export default Card;
