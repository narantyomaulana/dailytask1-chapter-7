import React from "react";
const Header = (props) => {
  const { title } = props;

  return (
    <div className="header">
      <div className="card-body">
        <h2 className="card-title">{title}</h2>
      </div>
    </div>
  )
}

export default Header;